import { NgModule } from '@angular/core';
import { MaterialModule } from "../../material.module";
import { CardComponent } from "./card.component";
import { CommonModule } from "@angular/common";


@NgModule({
  declarations: [
    CardComponent
  ],
  imports: [
    MaterialModule,
    CommonModule
  ],
  exports: [
    CardComponent
  ]
})
export class AnavaiCardModule { }
