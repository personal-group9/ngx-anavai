import { Component, Input } from '@angular/core';

@Component({
  selector: 'anavai-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {

  @Input() iconTitle?: string;
  @Input() cardTitle?: string;
  @Input() subtitle?: string;

}
