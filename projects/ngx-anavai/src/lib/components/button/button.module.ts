import { NgModule } from '@angular/core';
import { ButtonComponent } from "./button.component";
import { MaterialModule } from "../../material.module";


@NgModule({
  declarations: [
    ButtonComponent
  ],
  imports: [
    MaterialModule
  ],
  exports: [
    ButtonComponent
  ]
})
export class AnavaiButtonModule { }
