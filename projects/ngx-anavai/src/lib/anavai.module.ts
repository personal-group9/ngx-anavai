import { NgModule } from '@angular/core';
import { AnavaiButtonModule } from "./components/button/button.module";
import { MaterialModule } from "./material.module";
import { AnavaiCardModule } from "./components/card/card.module";
import { CommonModule } from "@angular/common";


@NgModule({
  imports: [
    MaterialModule,
    CommonModule
  ],
  exports: [
    AnavaiButtonModule,
    AnavaiCardModule
  ]
})
export class AnavaiModule { }
